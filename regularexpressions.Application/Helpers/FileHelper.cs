﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace regularexpressions.Application.Helpers
{
    public class FileHelper : IFileHelper
    {
        readonly string _imagePath;

        public FileHelper()
        {
            _imagePath = Path.Combine(Environment.CurrentDirectory, "images");
        }

        public string GetFileName(string url)
        {
            string pattern = @"(?<=\/)[^\/\?#]+(?=[^\/]*$)";

            var match = Regex.Match(url, pattern);

            return match.Success
                ? match.Value
                : null;
        }

        public string[] GetImagesPath()
        {
            return Directory.GetFiles(_imagePath);
        }

        public async Task SaveImageToFileAsync(byte[] image, string url)
        {
            var fileName = GetFileName(url);

            var filePath = Path.Combine(_imagePath, fileName);

            if (!Directory.Exists(_imagePath))
            {
                Directory.CreateDirectory(_imagePath);
            }

            using (var newFile = File.Create(filePath))
            {
                await newFile.WriteAsync(image);
            }
        }
    }
}
