﻿using System.Net.Http;
using System.Threading.Tasks;

namespace regularexpressions.Application.Helpers
{
    public class HttpHelper : IHttpHelper
    {
        private readonly HttpClient _httpClient;

        public HttpHelper()
        {
            _httpClient = new HttpClient();
        }

        public async Task<byte[]> GetImageAsync(string url)
        {
            using (var response = await _httpClient.GetAsync(url))
            {
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }

                return await response.Content.ReadAsByteArrayAsync();
            }
        }

        public async Task<string> GetPageAsync(string url)
        {
            var response = await _httpClient.GetAsync(url);

            return await response.Content.ReadAsStringAsync();
        }
    }
}
