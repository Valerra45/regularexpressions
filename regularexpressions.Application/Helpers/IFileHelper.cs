﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace regularexpressions.Application.Helpers
{
    public interface IFileHelper
    {
        Task SaveImageToFileAsync(byte[] stream, string url);

        string[] GetImagesPath();

        string GetFileName(string url);
    }
}
