﻿using System.Threading.Tasks;

namespace regularexpressions.Application.Helpers
{
    public interface IHttpHelper
    {
        Task<string> GetPageAsync(string url);

        Task<byte[]> GetImageAsync(string url);
    }
}
