﻿using System.Collections.Generic;

namespace regularexpressions.Application.Helpers
{
    public interface IParserHelper
    {
        List<string> GetImagesUrl(string page, string url);
    }
}
