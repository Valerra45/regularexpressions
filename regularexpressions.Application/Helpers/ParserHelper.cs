﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace regularexpressions.Application.Helpers
{
    public class ParserHelper : IParserHelper
    {
        public List<string> GetImagesUrl(string page, string url)
        {
            const string pattern = @"<img.*?src=""(http(?s).*?)""|<img.*?src=""(.*?)""";

            var matches = Regex.Matches(page, pattern, RegexOptions.Multiline);

            return matches
                .Select(x => string.IsNullOrEmpty(x.Groups[1].Value)
                ? url + x.Groups[2].Value.ToString()
                : x.Groups[1].Value.ToString())
                .ToList();
        }
    }
}
