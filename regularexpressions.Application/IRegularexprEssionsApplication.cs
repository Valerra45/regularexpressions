﻿using System.Threading.Tasks;

namespace regularexpressions.Application
{
    public interface IRegularexprEssionsApplication
    {
        Task DownloadImagesToFile(string url);

        string[] GetImagesPath();
    }
}
