﻿using regularexpressions.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace regularexpressions.Application
{
    public class RegularexprEssionsApplication : IRegularexprEssionsApplication
    {
        private readonly IHttpHelper _httpHelper;
        private readonly IParserHelper _parserHelper;
        private readonly IFileHelper _fileHelper;

        public RegularexprEssionsApplication(IHttpHelper httpHelper,
            IParserHelper parserHelper,
            IFileHelper fileHelper)
        {
            _httpHelper = httpHelper;
            _parserHelper = parserHelper;
            _fileHelper = fileHelper;
        }

        public async Task DownloadImagesToFile(string url)
        {
            var page = await _httpHelper.GetPageAsync(url);

            var urls = _parserHelper.GetImagesUrl(page, url);

            await SaveImagesAsync(urls);
        }

        public string[] GetImagesPath()
        {
            return _fileHelper.GetImagesPath();
        }

        private async Task SaveImagesAsync(List<string> urls)
        {
            foreach (var url in urls)
            {
                var image = await _httpHelper.GetImageAsync(url);

                if (image != null)
                {
                    await _fileHelper.SaveImageToFileAsync(image, url);
                }
            }
        }


    }
}
