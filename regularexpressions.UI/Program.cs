﻿using regularexpressions.Application;
using regularexpressions.Application.Helpers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace regularexpressions.UI
{
    class Program
    {
        private static int _arrow = 0;
        private static string[] _menu =
        {
            "Load files",
            "Show files",
            "Exit"
        };

        static async Task Main(string[] args)
        {
            var httpHelper = new HttpHelper();
            var parserHelper = new ParserHelper();
            var fileHelper = new FileHelper();

            var app = new RegularexprEssionsApplication(httpHelper, parserHelper, fileHelper);

            while (true)
            {
                Console.Clear();

                if (_arrow < (int)Menu.Url)
                {
                    _arrow = (int)Menu.Exit;
                }
                else if (_arrow > (int)Menu.Exit)
                {
                    _arrow = (int)Menu.Url;
                }

                for (int i = 0; i < _menu.Length; i++)
                {
                    Console.WriteLine($"{(_arrow == i ? ">>" : "  ")} {_menu[i]}");
                }

                ConsoleKey key = Console.ReadKey().Key;

                if (key == ConsoleKey.UpArrow)
                {
                    _arrow--;
                }
                else if (key == ConsoleKey.DownArrow)
                {
                    _arrow++;
                }
                else if (key == ConsoleKey.Enter)
                {
                    switch ((Menu)_arrow)
                    {
                        case Menu.Url:
                            Console.Clear();
                            Console.Write("Enter url to download: ");

                            var url = Console.ReadLine();

                            Console.Clear();
                            Console.WriteLine("Download images...");

                            try
                            {
                                await app.DownloadImagesToFile(url);
                                Console.Clear();
                                Console.WriteLine("Downloading images completed.");
                                Thread.Sleep(1000);
                            }
                            catch (Exception ex)
                            {
                                Console.Clear();
                                Console.WriteLine(ex.Message);
                                Thread.Sleep(2000);
                            }

                            break;
                        case Menu.Files:
                            try
                            {
                                var imagesPath = app.GetImagesPath();

                                Console.Clear();
                                Console.WriteLine("Downloaded files:");

                                foreach (var path in imagesPath)
                                {
                                    Console.WriteLine(path);
                                }

                                Console.WriteLine();
                                Console.WriteLine("Press any key to continue...");
                                Console.ReadKey();
                            }
                            catch (Exception ex)
                            {
                                Console.Clear();
                                Console.WriteLine(ex.Message);
                                Thread.Sleep(2000);
                            }

                            break;
                        case Menu.Exit:
                            Console.Clear();
                            return;
                    }

                }
            }
        }

        private enum Menu
        {
            Url,
            Files,
            Exit
        }
    }
}
